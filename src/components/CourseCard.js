import {Row, Col, Card} from 'react-bootstrap'

export default function CourseCard() {
	return(
		<Row className = "mt-1 mb-1">
			<Col xs = {12} md = {12}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h4>React-BootStrap</h4>
						</Card.Title>
						<Card.Text>
							<h5>Description</h5>
							<p>This is a sample course offering</p><br/>
						
							<p className = "mb-0">Price</p>
							<p>PHP 43,000</p>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
